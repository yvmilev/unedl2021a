﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSuma_Click(object sender, EventArgs e)
        {
            double n1 = double.Parse(textBoxV1.Text);
            double n2 = double.Parse(textBoxV2.Text);
            double total = n1 + n2;
            labelResultado.Text = String.Concat(total);
        }

        private void buttonResta_Click(object sender, EventArgs e)
        {
            double n1 = double.Parse(textBoxV1.Text);
            double n2 = double.Parse(textBoxV2.Text);
            double total = n1 - n2;
            labelResultado.Text = String.Concat(total);
        }

        private void buttonMultiplicacion_Click(object sender, EventArgs e)
        {
            double n1 = double.Parse(textBoxV1.Text);
            double n2 = double.Parse(textBoxV2.Text);
            double total = n1 * n2;
            labelResultado.Text = String.Concat(total);
        }

        private void buttonDivison_Click(object sender, EventArgs e)
        {
            if ((double.Parse(textBoxV2.Text) == 0) || (double.Parse(textBoxV1.Text) == 0))
            {
                MessageBox.Show("No es posibe dividir entre 0.");
                textBoxV1.Clear();
                textBoxV2.Clear();
            }
            else
            {
                double n1 = double.Parse(textBoxV1.Text);
                double n2 = double.Parse(textBoxV2.Text);
                double total = n1 / n2;
                labelResultado.Text = String.Concat(total);
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBoxV1.Clear();
            textBoxV2.Clear();
            labelResultado.Text = "0";
        }
    }
}
