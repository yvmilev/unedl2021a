﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Ventana : Form
    {
        public Ventana()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            {
                string filepath = "C:/Users/yamil/Desktop/segundo_examen_parcial_parametros.txt";
                List<Class> personas = new List<Class>();
                List<string> lines = File.ReadAllLines(filepath).ToList();
                foreach (var line in lines)
                {
                    string[] entries = line.Split();
                    Class x = new Class();
                    x.dato = entries[0];
                    personas.Add(x);
                }
                foreach (var dat in personas)
                {
                    Box1.Items.Add($"{dat.dato}");
                }
            }

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            String line;
            try
            {
                StreamReader sr = new StreamReader("C:/Users/yamil/Desktop/segundo_examen_parcial_parametros.txt");
                line = sr.ReadLine();
                while (line != null)
                {
                    Console.WriteLine(line);
                    line = sr.ReadLine();
                }
                sr.Close();
                Console.ReadLine();
            }
            catch (Exception a)
            {
                Box1.Items.Add("Exception: " + a.Message);
            }
            finally
            {
                Box1.Items.Add("Executing finally block.");
            }
        }
    


        private void Box1_Enter(object sender, EventArgs e)
        {
        }

        private void Ventana_Load(object sender, EventArgs e)
        {
        }

        private void Box1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}
